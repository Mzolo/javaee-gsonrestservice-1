package zikak.net.tests.model;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
/*
 * The @Service annotation is a specialization of the @Component annotation.
 * The @Component annotation marks a java class as a bean so the component-scanning mechanism
 * of Spring can pick it up and pull it into the application context
 */
@Service
public class Writer {
    private String name;
    private String location;

    // constructors
    public Writer() {
    }

    public Writer(String name, String location) {
        this.name = name;
        this.location = location;
    }

    public String getName() {
        return name;
    }
    // Getter and Setter
    public void setName(String name) {
        this.name = name;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public List<Writer> getWriters() {
        List<Writer> list = new ArrayList<>();
        list.add(new Writer("Jimi", "London"));
        list.add(new Writer("Miles", "Detroit"));
        list.add(new Writer("Fred","Paris"));
        list.add(new Writer("John", "London"));
        list.add(new Writer("John", "Sydney"));
        return list;
    }
} // end of class
