package zikak.net.tests.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import zikak.net.tests.model.Writer;

import java.util.List;

@RestController
public class BlogController {
    // Field-base DI - not recommended with Spring MVC
    @Autowired
    private Writer writer;

    @RequestMapping(value = "/blog")
    public List<Writer> getBlog() {
        return writer.getWriters();
    }


} // end of class
